from odoo import models, fields, api


class VehicleWashWizard(models.TransientModel):
    _name = "vehicle.wash.wizard"

    def action_open_token_screen(self):
        return {
            "type": "ir.actions.act_url",
            "url": "/vehicle-wash/token-screen",
            "target": "new",
        }

    def action_open_screen1(self):
        return {
            "type": "ir.actions.act_url",
            "url": "/vehicle-wash/screen1",
            "target": "new",
        }

    def action_open_screen2(self):
        return {
            "type": "ir.actions.act_url",
            "url": "/vehicle-wash/screen2",
            "target": "new",
        }
