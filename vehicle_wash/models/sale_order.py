# -*- coding: utf-8 -*-

# import time
# from odoo import api, fields, models

# SALE_ORDER_STATE = [
#     ("wait1", "Awaiting Step 1"),
#     ("wait2", "In Progress"),
#     ("draft", "Quotation"),
#     ("sent", "Quotation Sent"),
#     ("done", "Finished"),
#     ("sale", "Sales Order"),
#     ("cancel", "Cancelled"),
# ]


# class SaleOrder(models.Model):
#     _inherit = "sale.order"

#     is_vehicle_wash_order = fields.Boolean(readonly=True, string="Garage sale")
#     vehicle_type = fields.Selection(
#         selection=[("small", "Small"), ("large", "Large")],
#     )
#     state = fields.Selection(
#         selection=SALE_ORDER_STATE,
#         string="Status",
#         readonly=True,
#         copy=False,
#         index=True,
#         tracking=3,
#         default="draft",
#     )

#     def action_start_job(self):
#         self.write({"state": "wait2"})

#     def action_finish_job(self):
#         self.write({"state": "draft"})
#         self.action_confirm()

#     # def print_paid_token(self):
#     #     return {
#     #         "type": "ir.actions.act_url",
#     #         "url": f"/vehicle-wash/paid-token?vehicle={self.vehicle}&date={time.strftime('%l:%M%p %Z on %b %d, %Y').strip()}",
#     #         "target": "new",
#     #     }

#     def create_order_from_vehicle_wash(self, order_data):
#         partner = None
#         if "number" in order_data:
#             partner = self.env["res.partner"].search_read(
#                 [("plate_number", "=", order_data["number"])]
#             )
#         if not partner:
#             partner = self.env["res.partner"].search_read(
#                 [("company_number", "=", order_data["number"])]
#             )
#         if not partner:
#             partner = self.env["res.partner"].create(
#                 {"name": order_data["number"], "plate_number": order_data["number"]}
#             )
#         if partner:
#             vehicle_wash_product = self.env["product.template"].search_read(
#                 [("name", "ilike", "Vehicle Wash")]
#             )
#             if not vehicle_wash_product:
#                 return False
#             order_data = {
#                 "is_vehicle_wash_order": True,
#                 "vehicle_type": order_data["vehicle_type"],
#                 "vehicle": order_data["number"],
#                 "partner_id": partner[0]["id"],
#                 "state": "wait1",
#             }
#             new_order = self.env["sale.order"].create(order_data)
#             new_line = self.env["sale.order.line"].create(
#                 {
#                     "product_id": vehicle_wash_product[0]["id"],
#                     "name": vehicle_wash_product[0]["name"],
#                     "order_id": new_order.id,
#                     "product_uom_qty": 1,
#                 }
#             )
#             return new_order.name
#         return False
