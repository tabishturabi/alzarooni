# -*- coding: utf-8 -*-

import uuid
from odoo import api, fields, models


class ResPartner(models.Model):
    _inherit = "res.partner"

    plate_number = fields.Char(required=False)
    company_number = fields.Char(
        required=False, compute="_compute_company_number", store=True
    )

    def _get_unique_company_number(self, record_id):
        unique_id = uuid.uuid4().hex
        unique_id = "".join([i for i in unique_id if not i.isdigit()])
        record_id = "".join([i for i in str(record_id) if i.isdigit()])
        return (record_id + unique_id)[:6]

    @api.depends("is_company")
    def _compute_company_number(self):
        for record in self:
            if record.is_company:
                if not record.company_number:
                    record.company_number = self._get_unique_company_number(
                        record.id
                    ).upper()
            else:
                record.company_number = ""