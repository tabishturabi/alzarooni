# -*- coding: utf-8 -*-

import json
from odoo import http


class VehicleWashController(http.Controller):
    @http.route("/vehicle-wash/token-screen", auth="public")
    def vehicle_wash_token_screen(self, **kw):
        return http.request.render("vehicle_wash.vehicle_wash_token_screen")

    @http.route("/vehicle-wash/get-token", auth="public", type="json", csrf=False)
    def vehicle_wash_get_token(self, **kw):
        data = json.loads(http.request.httprequest.data)
        token = (
            http.request.env["sale.order"]
            .sudo()
            .create_order_from_vehicle_wash(
                {
                    "number": data.get("plateNumber"),
                    "vehicle_type": data.get("vehicleType"),
                }
            )
        )
        print(token)
        return {"token": token.replace("S", "")}

    @http.route("/vehicle-wash/screen1", auth="public")
    def vehicle_wash_screen1(self, **kw):
        return http.request.render("vehicle_wash.vehicle_wash_screen1")

    @http.route(
        "/vehicle-wash/get-next-customer", auth="public", type="json", csrf=False
    )
    def vehicle_wash_get_next_customer(self, **kw):
        data = json.loads(http.request.httprequest.data)
        state = "step1" if data.get("step") == "1" else "step2"
        token = (
            http.request.env["sale.order"]
            .sudo()
            .search_read(
                [("is_vehicle_wash_order", "=", True), ("state", "=", state)],
                ["name"],
                order="create_date desc",
                limit=1,
            )
        )
        print(token)
        if len(token) > 0:
            token = token[0]["name"].replace("S", "")
        else:
            token = ""
        return {"token": token}

    @http.route("/vehicle-wash/screen2", auth="public")
    def vehicle_wash_screen2(self, **kw):
        return http.request.render("vehicle_wash.vehicle_wash_screen2")

    @http.route("/vehicle-wash/paid-token", auth="public")
    def vehicle_wash_print_token(self, **kw):
        return http.request.render("vehicle_wash.vehicle_wash_paid_token")
