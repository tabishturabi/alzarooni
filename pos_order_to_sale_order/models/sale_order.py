# Copyright (C) 2017 - Today: GRAP (http://www.grap.coop)
# @author: Sylvain LE GAL (https://twitter.com/legalsylvain)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import Command, _, api, models


class SaleOrder(models.Model):
    _inherit = "sale.order"

    @api.model
    def _prepare_from_pos(self, order_data):
        PosSession = self.env["pos.session"]
        session = PosSession.browse(order_data["pos_session_id"])
        ResPartner = self.env["res.partner"]
        partner = ResPartner.browse(order_data["partner_id"])
        SaleOrderLine = self.env["sale.order.line"]
        order_lines = [
            Command.create(SaleOrderLine._prepare_from_pos(sequence, line_data[2]))
            for sequence, line_data in enumerate(order_data["lines"], start=1)
        ]
        vehicleNo = ''
        driverNo = ''
        if order_data.get('lines'):
            for line in order_data.get('lines'):
                if line[2].get('vehicleNo'):
                    vehicleNo = line[2].get('vehicleNo')
                if line[2].get('driverNo'):
                    driverNo = line[2].get('driverNo')
                print("sadas")
        return {
            "pump": order_data["pump"],
            "operator": order_data["operator"],
            "vehicle": order_data["vehicle"],
            "driver_phone": driverNo,
            "partner_id": order_data["partner_id"],
            "origin": _("Point of Sale %s") % (session.name),
            "client_order_ref": order_data["name"],
            "user_id": partner.user_id.id,
            "pricelist_id": order_data["pricelist_id"],
            "fiscal_position_id": order_data["fiscal_position_id"],
            "order_line": order_lines,
        }

    @api.model
    def create_order_from_pos(self, order_data, action):
        # Create Draft Sale order
        order_vals = self._prepare_from_pos(order_data)
        sale_order = self.with_context(
            pos_order_lines_data=[x[2] for x in order_data.get("lines", [])]
        ).create(order_vals)
        if (
            sale_order.partner_credit_warning
            and sale_order.partner_credit_warning != ""
        ):
            sale_order.action_confirm()
            sale_order.mapped("picking_ids").button_validate()
            return {
                "error": sale_order.partner_credit_warning
                or "This customer has exceeded their credit limit."
            }

        # Confirm Sale Order
        if action in ["confirmed", "delivered", "invoiced"]:
            sale_order.action_confirm()
            # Mark all moves are delivered
            for move in sale_order.mapped("picking_ids.move_ids_without_package"):
                move.quantity = move.product_uom_qty
            sale_order.mapped("picking_ids").button_validate()

        if action in ["invoiced"]:
            # Create and confirm invoices
            invoices = sale_order._create_invoices()
            invoices.action_post()

        return {
            "sale_order_id": sale_order.id,
        }
