{
    "name": "Accounting Enhancement",
    "summary": "Accounting Enhancement",
    "description": """Long description""",
    "category": "Accounting",
    "version": "17.0",
    "depends": ["base", "account", "stock"],
    "data": [
        'security/ir.model.access.csv',
        'data/sequence_data.xml',
        'views/view_bank_statement_tree.xml',
        'views/stock_picking_views.xml',
        'views/internal_usage_views.xml',
        'views/stock_location_views.xml',
        'wizards/stock_warn_insufficient_qty.xml',
    ],
}
