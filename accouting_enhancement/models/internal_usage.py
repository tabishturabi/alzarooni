from odoo import fields, models, api, _
from odoo.tools import float_is_zero
from odoo.exceptions import UserError


class InternalUsage(models.Model):
    _name = 'internal.usage'
    _inherit = ['mail.thread']
    _description = "Internal Usage"
    _order = 'id desc'

    name = fields.Char('Reference', default=lambda self: _('New'), copy=False, readonly=True, required=True)
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.company, required=True)
    origin = fields.Char(string='Source Document')
    product_id = fields.Many2one(
        'product.product', 'Product', domain="[('type', 'in', ['product', 'consu'])]",
        required=True, check_company=True)
    product_uom_id = fields.Many2one(
        'uom.uom', 'Unit of Measure',
        compute="_compute_product_uom_id", store=True, readonly=False, precompute=True,
        required=True, domain="[('category_id', '=', product_uom_category_id)]")
    product_uom_category_id = fields.Many2one(related='product_id.uom_id.category_id')
    tracking = fields.Selection(string='Product Tracking', readonly=True, related="product_id.tracking")
    lot_id = fields.Many2one(
        'stock.lot', 'Lot/Serial',
        domain="[('product_id', '=', product_id)]", check_company=True)
    package_id = fields.Many2one(
        'stock.quant.package', 'Package',
        check_company=True)
    owner_id = fields.Many2one('res.partner', 'Owner', check_company=True)
    move_ids = fields.One2many('stock.move', 'usage_id')
    picking_id = fields.Many2one('stock.picking', 'Picking', check_company=True)
    location_id = fields.Many2one(
        'stock.location', 'Source Location', store=True, required=True, precompute=True,
        domain="[('usage', '=', 'internal')]", check_company=True, readonly=False)
    usage_location_id = fields.Many2one(
        'stock.location', 'Usage Location',
        compute='_compute_usage_location_id', store=True, required=True, precompute=True,
        domain="[('internal_usage_location', '=', True)]", check_company=True, readonly=False)
    usage_qty = fields.Float(
        'Quantity', compute='_compute_usage_qty', required=True, digits='Product Unit of Measure', default=1.0,
        readonly=False, store=True)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('done', 'Done')],
        string='Status', default="draft", readonly=True, tracking=True)
    date_done = fields.Datetime('Date', readonly=True)

    # should_replenish = fields.Boolean(string='Replenish Quantities')

    def action_get_stock_move_lines(self):
        action = self.env['ir.actions.act_window']._for_xml_id('stock.stock_move_line_action')
        action['domain'] = [('move_id', 'in', self.move_ids.ids)]
        return action

    def action_get_stock_picking(self):
        action = self.env['ir.actions.act_window']._for_xml_id('stock.action_picking_tree_all')
        action['domain'] = [('id', '=', self.picking_id.id)]
        return action

    def _should_check_available_qty(self):
        return self.product_id.type == 'product'

    def check_available_qty(self):
        if not self._should_check_available_qty():
            return True

    @api.depends('move_ids', 'move_ids.move_line_ids.quantity', 'product_id')
    def _compute_usage_qty(self):
        self.usage_qty = 1
        for usage in self:
            if usage.move_ids:
                usage.usage_qty = usage.move_ids[0].quantity

    def _prepare_move_values(self):
        self.ensure_one()
        return {
            'name': self.name,
            'origin': self.origin or self.picking_id.name or self.name,
            'company_id': self.company_id.id,
            'product_id': self.product_id.id,
            'product_uom': self.product_uom_id.id,
            'state': 'draft',
            'product_uom_qty': self.usage_qty,
            'location_id': self.location_id.id,
            'scrapped': True,
            'usage_id': self.id,
            'location_dest_id': self.usage_location_id.id,
            'move_line_ids': [(0, 0, {
                'product_id': self.product_id.id,
                'product_uom_id': self.product_uom_id.id,
                'quantity': self.usage_qty,
                'location_id': self.location_id.id,
                'location_dest_id': self.usage_location_id.id,
                'package_id': self.package_id.id,
                'owner_id': self.owner_id.id,
                'lot_id': self.lot_id.id,
            })],
            'picked': True,
            'picking_id': self.picking_id.id
        }

    def do_internal_usage(self):
        self._check_company()
        for usage in self:
            usage.name = self.env['ir.sequence'].next_by_code('internal.usage') or _('New')
            move = self.env['stock.move'].create(usage._prepare_move_values())
            move._action_done()
            usage.write({'state': 'done'})
            usage.date_done = fields.Datetime.now()
            # if usage.should_replenish:
            #     usage.do_replenish()
        return True

    @api.depends('product_id')
    def _compute_product_uom_id(self):
        for usage in self:
            usage.product_uom_id = usage.product_id.uom_id

    @api.depends('company_id')
    def _compute_usage_location_id(self):
        self.usage_location_id = None
        groups = self.env['stock.location']._read_group(
            [('company_id', 'in', self.company_id.ids), ('internal_usage_location', '=', True)], ['company_id'],
            ['id:min'])
        locations_per_company = {
            company.id: stock_warehouse_id
            for company, stock_warehouse_id in groups
        }
        for usage in self:
            usage.usage_location_id = locations_per_company[usage.company_id.id]

    def action_validate(self):
        self.ensure_one()
        if float_is_zero(self.usage_qty,
                         precision_rounding=self.product_uom_id.rounding):
            raise UserError(_('You can only enter positive quantities.'))
        if self.check_available_qty():
            return self.do_internal_usage()
        else:
            ctx = dict(self.env.context)
            ctx.update({
                'default_product_id': self.product_id.id,
                'default_location_id': self.location_id.id,
                'default_usage_id': self.id,
                'default_quantity': self.product_uom_id._compute_quantity(self.usage_qty, self.product_id.uom_id),
                'default_product_uom_name': self.product_id.uom_name
            })
            return {
                'name': self.product_id.display_name + _(': Insufficient Quantity To Internal Usage'),
                'view_mode': 'form',
                'res_model': 'stock.warn.insufficient.qty.usage',
                'view_id': self.env.ref('accouting_enhancement.stock_warn_insufficient_qty_usage_form_view').id,
                'type': 'ir.actions.act_window',
                'context': ctx,
                'target': 'new'
            }
