from odoo import models, fields, _


class StockMove(models.Model):
    _inherit = 'stock.move'

    usage_id = fields.Many2one('internal.usage', "Usage ID")
