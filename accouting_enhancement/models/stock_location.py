from odoo import models, fields, _


class StockLocation(models.Model):
    _inherit = 'stock.location'

    internal_usage_location = fields.Boolean('Is a Internal Usage Location?', default=False,
                                             help='Check this box to allow using this location to put Internal Usage goods.')
