{
    "name": "NFC Login",
    "summary": "This module allows users login / logout to a POS session with an NFC card.",
    "description": """Long description""",
    "author": "Evans Ehiorobo",
    "website": "https://www.fiverr.com/evansehiorobo",
    "category": "Sales/Point of Sale",
    "version": "0.1",
    "depends": ["base", "point_of_sale", "hr", "account", "stock", "sale_management","pos_order_to_sale_order"],
    "data": [
        "security/ir.model.access.csv",
        "views/views.xml",
        "views/dispenser_views.xml",
        "views/sale_diesel_garage_order.xml",
        "views/sale_menu.xml",
        "views/res_company.xml",
        "views/sale_view.xml",
        "views/sale_report.xml",
        "report/company_report.xml",
    ],
    "assets": {
        "point_of_sale._assets_pos": [
            "nfc_login/static/src/**/*",
        ],
    },
}
