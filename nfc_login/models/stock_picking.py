# -*- coding: utf-8 -*-
import hashlib
from odoo import api, fields, models


class StockPicking(models.Model):
    _inherit = "stock.picking"

    dispenser_pump = fields.Char(
        string="Dispenser", compute="_compute_pos_sales_data", store=True
    )
    operator = fields.Many2one(
        "hr.employee", check_company=True, compute="_compute_pos_sales_data", store=True
    )
    vehicle = fields.Char(compute="_compute_pos_sales_data", store=True,string="Vehicle No")
    driver_name = fields.Char("Driver Name",related='sale_id.driver_name')
    driver_phone = fields.Char("Driver Phone",related='sale_id.driver_phone')
    total_quantity = fields.Float(
        compute="_compute_total_quantity_and_price", store=True
    )
    single_order_line_uom = fields.Char(
        string="UoM", compute="_compute_total_quantity_and_price", store=True
    )
    single_order_line_price = fields.Float(
        string="Price", compute="_compute_total_quantity_and_price", store=True
    )
    location_id = fields.Many2one(
        "stock.location",
        "Source Location",
        compute="_compute_location_id_from_dispenser",
        store=True,
        precompute=True,
        readonly=False,
        check_company=True,
        required=True,
    )
    location_dest_id = fields.Many2one(
        "stock.location",
        "Destination Location",
        compute="_compute_location_id_from_dispenser",
        store=True,
        precompute=True,
        readonly=False,
        check_company=True,
        required=True,
    )
    test_location_id = fields.Many2one(
        "stock.location",
        "Source Location",
        compute="_compute_location_id_from_dispenser",
        readonly=False,
        check_company=True,
        required=True,
    )

    @api.depends("move_ids")
    def _compute_total_quantity_and_price(self):
        for record in self:
            if record.move_ids and len(record.move_ids) > 0:
                record.total_quantity = record.move_ids[0].product_uom_qty
                record.single_order_line_price = record.move_ids[0].price_unit
                record.single_order_line_uom = record.move_ids[0].product_uom.name
            else:
                record.total_quantity = 0
                record.single_order_line_price = 0
                record.single_order_line_uom = ""

    @api.depends("group_id")
    def _compute_pos_sales_data(self):
        for record in self:
            if record.group_id:
                sale_order = record.group_id.sale_id
                record.dispenser_pump = sale_order.pump.name
                record.operator = sale_order.operator
                record.vehicle = sale_order.vehicle
            else:
                record.dispenser_pump = False
                record.operator = False
                record.vehicle = ""

    @api.model
    def default_get(self, fields_list):
        defaults = super(StockPicking, self).default_get(fields_list)
        delivery_type = self.env.ref('stock.picking_type_out')
        active_id = self.env.context.get('active_id')
        if active_id == delivery_type.id:
            if 'picking_type_id' in defaults:
                defaults['picking_type_id'] = None
        return defaults

    @api.depends("origin", "picking_type_id", "partner_id")
    def _compute_location_id_from_dispenser(self):
        for picking in self:
            if picking.origin:
                origin_sale_order = self.env["sale.order"].search(
                    [("name", "=", picking.origin)], limit=1
                )
                if origin_sale_order and origin_sale_order[0].pump:
                    origin_sale_order = origin_sale_order[0]
                    origin_order_dispenser = origin_sale_order.pump
                    # self.env["pos.dispenser"].search(
                    #     [("name", "=", origin_sale_order.pump)], limit=1
                    # )
                    if origin_order_dispenser:
                        # origin_order_dispenser = origin_order_dispenser[0]
                        picking.location_id = origin_order_dispenser.location_id.id
                        picking.location_dest_id = origin_order_dispenser.location_id.id
                        self.env["stock.picking"].search(
                            [("name", "=", picking.name)], limit=1
                        )[0].write(
                            {"location_id": origin_order_dispenser.location_id.id}
                        )
                    continue
            if picking.state != "draft" or picking.return_id:
                continue
            picking = picking.with_company(picking.company_id)
            if picking.picking_type_id:
                if picking.picking_type_id.default_location_src_id:
                    location_id = picking.picking_type_id.default_location_src_id.id
                elif picking.partner_id:
                    location_id = picking.partner_id.property_stock_supplier.id
                else:
                    _customerloc, location_id = self.env[
                        "stock.warehouse"
                    ]._get_partner_locations()

                if picking.picking_type_id.default_location_dest_id:
                    location_dest_id = (
                        picking.picking_type_id.default_location_dest_id.id
                    )
                elif picking.partner_id:
                    location_dest_id = picking.partner_id.property_stock_customer.id
                else:
                    location_dest_id, _supplierloc = self.env[
                        "stock.warehouse"
                    ]._get_partner_locations()

                picking.location_id = location_id
                picking.location_dest_id = location_dest_id
