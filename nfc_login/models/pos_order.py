# -*- coding: utf-8 -*-
from odoo import fields, models


class PosOrderLine(models.Model):
    _inherit = "pos.order.line"

    customer_note = fields.Char("Vehicle")


class ResPartner(models.Model):
    _inherit = "res.partner"

    fleet_id = fields.Many2one("fleet.vehicle", string="Fleet")


class PosSession(models.Model):
    _inherit = 'pos.session'

    def _pos_ui_models_to_load(self):
        res = super(PosSession, self)._pos_ui_models_to_load()
        res.append("pos.dispenser")
        return res

    def _loader_params_sale_order(self):
        vals = super(PosSession, self)._loader_params_res_partner()
        vals["search_params"]["fields"].append("pump")
        return vals

    def _loader_params_pos_dispenser(self):
        vals = super(PosSession, self)._loader_params_res_partner()
        vals["search_params"]["fields"].append("name")
        return vals

    def _loader_params_pos_dispenser(self):
        return {
            "search_params": {
                "domain": [],
                "fields": ["name", "available_in_pos"],
            },
        }

    def _get_pos_ui_pos_dispenser(self, params):
        return self.env["pos.dispenser"].search_read(**params['search_params'])

    def _pos_data_process(self, loaded_data):
        res = super(PosSession, self)._pos_data_process(loaded_data)
        loaded_data["pump"] = {
            pump["id"]: pump for pump in loaded_data["pos.dispenser"]
        }
        return res
