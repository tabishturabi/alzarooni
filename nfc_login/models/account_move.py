# -*- coding: utf-8 -*-
import hashlib
from odoo import api, fields, models


class AccountMove(models.Model):
    _inherit = "account.move"

    dispenser_pump = fields.Char(
        string="Dispenser", compute="_compute_pos_sales_data", store=True
    )
    operator = fields.Many2one(
        "hr.employee", check_company=True, compute="_compute_pos_sales_data", store=True
    )
    vehicle = fields.Char(compute="_compute_pos_sales_data", store=True,string="Vehicle No")
    driver_name = fields.Char(compute="_compute_pos_sales_data",string="Driver Name", store=True)
    driver_phone = fields.Char(compute="_compute_pos_sales_data",string="Driver Phone", store=True)
    total_quantity = fields.Float(
        compute="_compute_total_quantity_and_price", store=True
    )
    single_order_line_uom = fields.Char(
        string="UoM", compute="_compute_total_quantity_and_price", store=True
    )
    single_order_line_price = fields.Float(
        string="Price", compute="_compute_total_quantity_and_price", store=True
    )

    @api.depends("line_ids")
    def _compute_total_quantity_and_price(self):
        for record in self:
            if record.line_ids and len(record.line_ids) > 0:
                record.total_quantity = record.line_ids[0].quantity
                record.single_order_line_price = record.line_ids[0].price_unit
                record.single_order_line_uom = record.line_ids[0].product_uom_id.name
            else:
                record.total_quantity = 0
                record.single_order_line_price = 0
                record.single_order_line_uom = ""

    @api.depends("line_ids")
    def _compute_pos_sales_data(self):
        for record in self:
            if record.line_ids:
                source_orders = self.line_ids.sale_line_ids.order_id
                if len(source_orders) > 0:
                    sale_order = source_orders[0]
                    record.dispenser_pump = sale_order.pump.name
                    record.operator = sale_order.operator
                    record.vehicle = sale_order.vehicle
                    record.driver_name = sale_order.driver_name
                    record.driver_phone = sale_order.driver_phone
                    continue
            record.dispenser_pump = False
            record.operator = False
            record.vehicle = ""
