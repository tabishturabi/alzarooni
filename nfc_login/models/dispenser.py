from odoo import models, fields


class Dispenser(models.Model):
    _name = "pos.dispenser"
    _description = "Point of Sale Dispenser"
    _sql_constraints = [
        ("name_unique", "unique(name)", "Choose another value - it has to be unique!")
    ]

    name = fields.Char(string="Name", required=True)
    config_id = fields.Many2one("pos.config", string="Point of Sale", required=True)
    location_id = fields.Many2one("stock.location", "Source Location", required=True)
    available_in_pos = fields.Boolean(default=True)
    sell_in_gallons = fields.Boolean(default=False)
