# -*- coding: utf-8 -*-
import hashlib
from odoo import api, fields, models


class HrEmployee(models.Model):
    _inherit = "hr.employee"

    key_card_code = fields.Char(compute="_compute_key_card_code")

    @api.depends("barcode")
    def _compute_key_card_code(self):
        for record in self:
            if record.barcode:
                h = hashlib.new("sha1")
                h.update(record.barcode.encode())
                record.key_card_code = (
                    self.env["ir.config_parameter"].get_param("web.base.url")
                    + "/pos/ui?config_id=1&direct-login="
                    + h.hexdigest()
                )
            else:
                record.key_card_code = ""
