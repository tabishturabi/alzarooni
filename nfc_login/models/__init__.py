from . import (
    account_move,
    dispenser,
    hr,
    pos_order,
    product_pricelist,
    sale_order,
    stock_picking,
    res_company,
)
