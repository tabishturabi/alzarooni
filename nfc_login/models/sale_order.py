# -*- coding: utf-8 -*-
from odoo import api, fields, models

SALE_ORDER_STATE = [
    ("wait1", "Awaiting Step 1"),
    ("wait2", "In Progress"),
    ("draft", "Quotation"),
    ("sent", "Quotation Sent"),
    ("done", "Finished"),
    ("sale", "Sales Order"),
    ("cancel", "Cancelled"),
]


class SaleOrder(models.Model):
    _inherit = "sale.order"

    dispenser_pump = fields.Char(compute="_compute_dispenser_pump", store=True)
    pump = fields.Many2one("pos.dispenser")
    operator = fields.Many2one("hr.employee", check_company=True)
    vehicle = fields.Char(string="Vehicle No",required=True)
    driver_name = fields.Char("Driver Name")
    driver_phone = fields.Char("Driver Phone")
    total_quantity = fields.Float(
        compute="_compute_total_quantity_and_price", store=True
    )
    single_order_line_uom = fields.Char(
        string="UoM", compute="_compute_total_quantity_and_price", store=True
    )
    single_order_line_price = fields.Float(
        string="Price", compute="_compute_total_quantity_and_price", store=True
    )
    is_vehicle_wash_order = fields.Boolean(readonly=True, string="Garage sale")
    vehicle_type = fields.Selection(
        selection=[("small", "Small"), ("large", "Large")],
    )
    state = fields.Selection(
        selection=SALE_ORDER_STATE,
        string="Status",
        readonly=True,
        copy=False,
        index=True,
        tracking=3,
        default="draft",
    )

    def action_start_job(self):
        self.write({"state": "wait2"})

    def action_finish_job(self):
        self.write({"state": "draft"})
        self.action_confirm()

    def create_order_from_vehicle_wash(self, order_data):
        partner = None
        if "number" in order_data:
            partner = self.env["res.partner"].search_read(
                [("plate_number", "=", order_data["number"])]
            )
        if not partner:
            partner = self.env["res.partner"].search_read(
                [("company_number", "=", order_data["number"])]
            )
        if not partner:
            partner = self.env["res.partner"].create(
                {"name": order_data["number"], "plate_number": order_data["number"]}
            )
        if partner:
            vehicle_wash_product = self.env["product.template"].search_read(
                [("name", "ilike", "Vehicle Wash")]
            )
            if not vehicle_wash_product:
                return False
            order_data = {
                "is_vehicle_wash_order": True,
                "vehicle_type": order_data["vehicle_type"],
                "vehicle": order_data["number"],
                "partner_id": partner[0]["id"],
                "state": "wait1",
            }
            new_order = self.env["sale.order"].create(order_data)
            new_line = self.env["sale.order.line"].create(
                {
                    "product_id": vehicle_wash_product[0]["id"],
                    "name": vehicle_wash_product[0]["name"],
                    "order_id": new_order.id,
                    "product_uom_qty": 1,
                }
            )
            return new_order.name
        return False
    

    def default_get(self, fields_list):
        defaults = super().default_get(fields_list)
        emp = self.env.user.employee_ids.filtered(lambda emp : emp.user_id ==self.env.user)
        if emp:
            defaults['operator'] = emp
        return defaults    

    @api.depends('partner_id')
    def _compute_user_id(self):
        for order in self:
            if order.partner_id and not (order._origin.id and order.user_id):
                order.user_id = (
                    order.partner_id.user_id
                    # or order.partner_id.commercial_partner_id.user_id
                    # or (self.user_has_groups('sales_team.group_sale_salesman') and self.env.user)
                )


    @api.depends("pump")
    def _compute_dispenser_pump(self):
        for record in self:
            record.dispenser_pump = record.pump.name if record.pump else ""

    @api.depends("order_line")
    def _compute_total_quantity_and_price(self):
        for record in self:
            if record.order_line and len(record.order_line) == 1:
                record.total_quantity = record.order_line[0].product_uom_qty
                record.single_order_line_price = record.order_line[0].price_unit
                record.single_order_line_uom = record.order_line[0].product_uom.name
            else:
                record.total_quantity = 0
                record.single_order_line_price = 0
                record.single_order_line_uom = ""

    @api.depends('company_id', 'partner_id', 'amount_total')
    def _compute_partner_credit_warning(self):
        for order in self:
            order.with_company(order.company_id)
            order.partner_credit_warning = ''
            show_warning = order.state in ('sale','draft', 'sent') and \
                           order.company_id.account_use_credit_limit
            if show_warning:
                order.partner_credit_warning = self.env['account.move']._build_credit_warning_message(
                    order,
                    current_amount=(order.amount_total / order.currency_rate),
                )