# -*- coding: utf-8 -*-
import hashlib
from odoo import api, fields, models


class ProductPricelistItem(models.Model):
    _inherit = "product.pricelist.item"

    litres_price = fields.Float(digits = (16,3))
    gallons_price = fields.Float(digits = (16,3))
