# -*- coding: utf-8 -*-
from odoo import fields, models


class Company(models.Model):
    _inherit = "res.company"

    other_company_details = fields.Html(string='New Company Details', translate=True)
    other_company_logo = fields.Binary(string="New Company Logo", readonly=False)