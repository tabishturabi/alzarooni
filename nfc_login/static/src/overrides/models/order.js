/** @odoo-module **/

import { Order } from "@point_of_sale/app/store/models";
import { _t } from "@web/core/l10n/translation";
import { patch } from "@web/core/utils/patch";
import { ErrorPopup } from "@point_of_sale/app/errors/popups/error_popup";
import { omit } from "@web/core/utils/objects";

patch(Order.prototype, {
    setup(_defaultObj, options) {
        super.setup(...arguments);
        this.partnerLitresPrice = 0;
        this.partnerGallonsPrice = 0;
    },

    async pay() {
        const vehicleLine = this.get_orderlines().find(
            (line) => !!line.getVehicleNo()
        );
        if (!vehicleLine) {
            await this.env.services.popup.add(ErrorPopup, {
                title: "Error",
                body: "You must set a Vehicle number",
                // sound: true,
            });
        }else if(!this.dispenser){
            await this.env.services.popup.add(ErrorPopup, {
                title: "Error",
                body: "You must set a Dispenser",
                // sound: true,
            });
        }
        else {
            return await super.pay(...arguments);
        }
    },

    export_for_printing() {
        var res = super.export_for_printing(...arguments);
        console.log("This Order===", this)
        if(res.headerData){
            res["headerData"]['customer_name'] = this.get_partner() ? this.get_partner().name : false
        }
        return res
    },
});
