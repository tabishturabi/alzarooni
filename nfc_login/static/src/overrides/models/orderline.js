/** @odoo-module */

import { patch } from "@web/core/utils/patch";
import { Orderline } from "@point_of_sale/app/store/models";

patch(Orderline.prototype, {
    setup() {
        super.setup(...arguments);
        this.vehicleNo = this.vehicleNo || "";
        this.driverNo = this.driverNo || "";
    },
    export_as_JSON() {
        const json = super.export_as_JSON.call(this);
        // Set the vehicleNo in the JSON object
        json.vehicleNo = this.vehicleNo;
        json.driverNo = this.driverNo;
        return json;
    },
    clone() {
        const orderline = super.clone();
        // Set the vehicleNo in the JSON object
        orderline.vehicleNo = this.vehicleNo;
        orderline.driverNo = this.driverNo;
        return orderline;
    },
    init_from_JSON(json) {
        super.init_from_JSON(...arguments);
        // Set the vehicleNo from the JSON data
        this.vehicleNo = json.vehicleNo || "";
        this.driverNo = json.driverNo || "";
    },
    setVehicleNo(vehicleNo) {
        this.vehicleNo = vehicleNo;
    },
    getVehicleNo() {
        return this.vehicleNo;
    },
    setDriverNo(driverNo) {
        this.driverNo = driverNo;
    },
    getDriverNo() {
        return this.driverNo;
    },
});
