/** @odoo-module */
import { PosStore } from "@point_of_sale/app/store/pos_store";
import { patch } from "@web/core/utils/patch";


patch(PosStore.prototype, {
    async setup() {
        const res = await super.setup(...arguments);
        this.dispenser = this.dispenser || false;
        return res;
    },
    setDispenser(dispenser) {
        this.dispenser = dispenser;
    },
    getDispenser() {
        return this.dispenser;
    },

    async push_orders_with_closing_popup(opts = {}) {
        try {
            await this.push_orders(opts);
            return true;
        } catch (error) {
            console.warn(error);
            const reason = this.failed
                ? _t(
                    'Some orders could not be submitted to ' +
                    'the server due to configuration errors. ' +
                    'You can exit the Point of Sale, but do ' +
                    'not close the session before the issue ' +
                    'has been resolved.'
                )
                : _t(
                    'Some orders could not be submitted to ' +
                    'the server due to internet connection issues. ' +
                    'You can exit the Point of Sale, but do ' +
                    'not close the session before the issue ' +
                    'has been resolved.'
                );
            await this.env.services.popup.add(ConfirmPopup, {
                title: _t('Offline Orders'),
                body: reason,
            });
            return false;
        }
    }
});
