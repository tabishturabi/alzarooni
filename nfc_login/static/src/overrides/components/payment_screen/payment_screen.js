/** @odoo-module */

import { PaymentScreen } from "@point_of_sale/app/screens/payment_screen/payment_screen";
import { patch } from "@web/core/utils/patch";
import { _t } from "@web/core/l10n/translation";
import { ErrorPopup } from "@point_of_sale/app/errors/popups/error_popup";

patch(PaymentScreen.prototype, {
    async _createSaleOrder(order_state, currentOrder) {
        const orderJson = currentOrder.export_as_JSON();
        orderJson["pump"] = currentOrder.dispenser.id;
        orderJson["operator"] = this.pos.get_cashier().id;
        orderJson["vehicle"] = currentOrder.orderlines[0].vehicleNo ?? "";
        orderJson["driverno"] = currentOrder.orderlines[0].driverno ?? "";
        debugger;
        if (orderJson["vehicle"] == "")
        {
         for (var key in currentOrder.orderlines){
        console.log( key, currentOrder.orderlines[key] );
        if (currentOrder.orderlines[key].vehicleNo)
        {
        orderJson["vehicle"] = currentOrder.orderlines[key].vehicleNo
        }
        if (currentOrder.orderlines[key].driverNo)
        {
        orderJson["driverno"] = currentOrder.orderlines[key].driverno
        }
        };

        }

        return await this.orm.call(
            "sale.order",
            "create_order_from_pos",
            [orderJson, order_state]
        );
    },

    async validateOrder(isForceValidate) {
        this.numberBuffer.capture();
        await this.currentOrder.export_for_printing()
        if (this.pos.config.cash_rounding) {
            if (!this.pos.get_order().check_paymentlines_rounding()) {
                this.popup.add(ErrorPopup, {
                    title: _t("Rounding error in payment lines"),
                    body: _t(
                        "The amount of your payment lines must be rounded to validate the transaction."
                    ),
                });
                return;
            }
        }
        if (await this._isOrderValid(isForceValidate)) {
            // remove pending payments before finalizing the validation
            for (const line of this.paymentLines) {
                if (!line.is_done()) {
                    this.currentOrder.remove_paymentline(line);
                }
            }
            const res = await this._createSaleOrder("confirmed", this.currentOrder);
            await this.currentOrder.export_for_printing()
            if (res["error"]) {
                this.popup.add(ErrorPopup, {
                    title: _t("Error validating order"),
                    body: _t(res["error"]),
                });
            }
            this.env.services.ui.unblock();
            await this.afterOrderValidation(true);
        }
    }
});
