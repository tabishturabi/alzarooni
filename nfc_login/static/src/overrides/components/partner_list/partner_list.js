/** @odoo-module */

import { PartnerListScreen } from "@point_of_sale/app/screens/partner_list/partner_list";
import { patch } from "@web/core/utils/patch";
import { useService } from "@web/core/utils/hooks";
import { ErrorPopup } from "@point_of_sale/app/errors/popups/error_popup";

patch(PartnerListScreen.prototype, {
    setup() {
        const res = super.setup(...arguments);
        this.popup = useService("popup");
        return res;
    },

    async confirm() {
        // if an order already exists, set the exported_order_for_printing field when the customer changes since
//        if (this.pos.get_order().export_for_printing) {
//            const order = this.pos.get_order();
//            setTimeout(() => order.exported_order_for_printing = order.export_for_printing(), 1000);
//        }

        if (
            this.state.selectedPartner?.property_product_pricelist &&
            this.state.selectedPartner.property_product_pricelist.length > 0
        ) {
            const pricelist = await this.orm.searchRead(
                "product.pricelist",
                [["id", "=", this.state.selectedPartner.property_product_pricelist[0]]],
            );
            const pricelistItems = await this.orm.searchRead(
                "product.pricelist.item",
                [["id", "in", pricelist[0].item_ids]],
            );
            this.pos.get_order().partnerLitresPrice = pricelistItems[0]?.litres_price ?? 0;
            this.pos.get_order().partnerGallonsPrice = pricelistItems[0]?.gallons_price ?? 0;
            const partner = await this.orm.searchRead(
                "sale.order",
                [["partner_id", "=", this.state.selectedPartner.id]],
                ["partner_credit_warning"],
                {
                    limit: 1,
                    order: 'create_date desc',
                }
            );
            if (partner.length > 0 && partner[0].partner_credit_warning) {
                this.popup.add(ErrorPopup, {
                    title: "Credit Limit Exceeded",
                    body: partner[0].partner_credit_warning,
                });
                if (this.pos.user?.role === "manager") {
                    this.props.resolve({ confirmed: true, payload: this.state.selectedPartner });
                    this.pos.closeTempScreen();
                }
                return;
            }
            this.props.resolve({ confirmed: true, payload: this.state.selectedPartner });
            this.pos.closeTempScreen();
        }
        else {
            this.props.resolve({ confirmed: true, payload: this.state.selectedPartner });
            this.pos.closeTempScreen();
        }
    },

    back(force = false) {
        if (this.state.detailIsShown && !force) {
            this.state.detailIsShown = false;
        } else {
            if (this.pos.user?.role === "manager") {
                this.props.resolve({ confirmed: false, payload: false });
                this.pos.closeTempScreen();
            }
        }
    },

    createPartner() {
        if (this.pos.user?.role === "manager") {
            // initialize the edit screen with default details about country, state, and lang
            const { country_id, state_id } = this.pos.company;
            this.state.editModeProps.partner = { country_id, state_id, lang: session.user_context.lang };
            this.activateEditMode();
        }
    },

    async clickPartner(partner) {
        if (this.state.selectedPartner && this.state.selectedPartner.id === partner.id) {
            this.state.selectedPartner = null;
        } else {
            this.state.selectedPartner = partner;
        }
        await this.confirm();
    },

    async saveChanges(processedChanges) {
        const partnerId = await this.orm.call("res.partner", "create_from_ui", [processedChanges]);
        await this.pos._loadPartners([partnerId]);
        this.state.selectedPartner = this.pos.db.get_partner_by_id(partnerId);
        await this.confirm();
    }
})
