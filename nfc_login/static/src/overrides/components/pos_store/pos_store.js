/** @odoo-module */

import { PosStore } from "@point_of_sale/app/store/pos_store";
import { patch } from "@web/core/utils/patch";
import { useService } from "@web/core/utils/hooks";

patch(PosStore.prototype, {
    async setup() {
        return await super.setup(...arguments);
        // const actualConfigId = localStorage.getItem("posConfigId");
        // const queryString = window.location.search;
        // const urlParams = new URLSearchParams(queryString);
        // const urlConfigId = urlParams.get("config_id");

        // if (actualConfigId && urlConfigId !== actualConfigId) {
        //     urlParams.set('config_id', actualConfigId);
        //     history.replaceState(null, '', '?' + urlParams + location.hash);
        //     location.reload();
        // }
        // else {
        //     localStorage.setItem("posConfigId", urlConfigId);
        //     await super.setup(...arguments);
        // }
    },
});

patch(PosStore.prototype, {
    async _processData(loadedData) {
        await super._processData(...arguments);
        this.pump = loadedData["pos.dispenser"];
    }
});