/** @odoo-module */

import { CashierName } from "@point_of_sale/app/navbar/cashier_name/cashier_name";
import { patch } from "@web/core/utils/patch";

patch(CashierName.prototype, {
    setup() {
        super.setup(...arguments);
        this.selectCashier = () => alert("hi");
    },
});
