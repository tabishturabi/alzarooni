/** @odoo-module */

import { _t } from "@web/core/l10n/translation";

import { LoginScreen } from "@pos_hr/app/login_screen/login_screen";
import { patch } from "@web/core/utils/patch";
import { useService } from "@web/core/utils/hooks";
import { NumberPopup } from "@point_of_sale/app/utils/input_popups/number_popup";
import { ErrorPopup } from "@point_of_sale/app/errors/popups/error_popup";
import { SelectionPopup } from "@point_of_sale/app/utils/input_popups/selection_popup";

patch(LoginScreen.prototype, {
    async lockPos() {
        this.pos.reset_cashier();
        localStorage.removeItem("direct-login-code");
    },

    async checkPin(employee) {
        const { confirmed, payload: inputPin } = await this.popup.add(NumberPopup, {
            isPassword: true,
            title: _t("Password?"),
        });

        if (!confirmed) {
            return false;
        }

        if (employee.pin !== Sha1.hash(inputPin)) {
            await this.popup.add(ErrorPopup, {
                title: _t("Incorrect Password"),
                body: _t("Please try again."),
            });
            return false;
        }
        return true;
    },

    /*
    * Verifies the pin if this employee has one, sets the employee as 
    * the current cashier, allows them to choose a customer, then
    * opens the POS products screen.
    */
    async setCashierOrManager(employee, dispenserList, employee_code = null) {
        if (!employee) {
            return await this.lockPos();
        }
        if (employee.pin && !(await this.checkPin(employee))) {
            return await this.lockPos();
        }
        this.pos.set_cashier(employee);
        if (employee_code) {
            localStorage.setItem("direct-login-code", employee_code);
        }

        const { confirmed, payload: newPartner } = await this.pos.showTempScreen(
            "PartnerListScreen"
        );
        if (confirmed) {
            this.pos.get_order().set_partner(newPartner);
        }
        await this.selectDispenser(dispenserList);
        this.back();
    },

    async selectDispenser(dispenserList) {
        if (!dispenserList.length) {
            return;
        }
        const { confirmed, payload: dispenser } = await this.popup.add(SelectionPopup, {
            title: _t("Change Dispenser"),
            list: dispenserList,
        });
        if (confirmed) {
            this.pos.get_order().setDispenser(dispenser);
        }
    },

    async setup() {
        super.setup(...arguments);
        const popup = useService("popup");
        const orm = useService("orm");
        this.popup = popup;
        this.orm = orm;
        // let dispenserList = await this.orm.searchRead(
        //     "pos.dispenser",
        //     [],
        // );
        let dispenserList = this.pos.pos_dispensers
            .filter((dispenser) => dispenser.available_in_pos)
            .map((dispenser) => {
                return {
                    id: dispenser.id,
                    item: dispenser,
                    label: dispenser.name,
                    isSelected: false,
                };
            });
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);

        if (urlParams.has('direct-login')) {
            const employee_code = urlParams.get("direct-login");
            urlParams.delete('direct-login');
            history.replaceState(null, '', '?' + urlParams + location.hash);
            // if the current logged in employee is the one using the card now,
            // log out and redirect to the lock screen
            if (localStorage.getItem("direct-login-code") === employee_code) {
                return await this.lockPos();
            }
            const employee = this.pos.employees.find((emp) => emp.barcode === employee_code);
            await this.setCashierOrManager(employee, dispenserList, employee_code);
            return;
        }
        else {
            if (this.pos.user?.role === "manager") {
                const employee = this.pos.employees.find((emp) => emp.user_id === this.pos.user.id);
                await this.setCashierOrManager(employee, dispenserList);
                return;
            }
        }
    },
});
