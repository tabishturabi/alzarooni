/** @odoo-module **/

import { TextAreaPopup } from "@point_of_sale/app/utils/input_popups/textarea_popup";
import { _t } from "@web/core/l10n/translation";
import { OrderlineCustomerNoteButton } from "@point_of_sale/app/screens/product_screen/control_buttons/customer_note_button/customer_note_button";
import { patch } from "@web/core/utils/patch";
import { Orderline } from "@point_of_sale/app/generic_components/orderline/orderline";

patch(OrderlineCustomerNoteButton.prototype, {
    async onClickVehicleNo() {
        const selectedOrderline = this.pos.get_order().get_selected_orderline();
        // FIXME POSREF can this happen? Shouldn't the orderline just be a prop?
        if (!selectedOrderline) {
            return;
        }
        const { confirmed, payload: vehicleNo } = await this.popup.add(TextAreaPopup, {
            startingValue: selectedOrderline.getVehicleNo(),
            title: _t("Vehicle No."),
        });

        if (confirmed) {
            selectedOrderline.setVehicleNo(vehicleNo);
        }
    },
    async onClickDriverNo() {
        const selectedOrderline = this.pos.get_order().get_selected_orderline();
        // FIXME POSREF can this happen? Shouldn't the orderline just be a prop?
        if (!selectedOrderline) {
            return;
        }
        const { confirmed, payload: driverNo } = await this.popup.add(TextAreaPopup, {
            startingValue: selectedOrderline.getDriverNo(),
            title: _t("Driver Phone."),
        });

        if (confirmed) {
            selectedOrderline.setDriverNo(driverNo);
        }
    }

});

Orderline.props = {
    ...Orderline.props,
    line: {
        shape: {
            driverNo: { type: String, optional: true },
            getDriverNo: { type: Function, optional: true },
            setDriverNo: { type: Function, optional: true },
            vehicleNo: { type: String, optional: true },
            getVehicleNo: { type: Function, optional: true },
            setVehicleNo: { type: Function, optional: true },
        },
    },
};