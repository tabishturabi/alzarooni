/** @odoo-module **/

import { _t } from "@web/core/l10n/translation";
import { ProductScreen } from "@point_of_sale/app/screens/product_screen/product_screen";
import { useService } from "@web/core/utils/hooks";
import { SelectionPopup } from "@point_of_sale/app/utils/input_popups/selection_popup";
import { Component } from "@odoo/owl";
import { usePos } from "@point_of_sale/app/store/pos_hook";

export class SetDispenserButton extends Component {
    static template = "product_multi_uom_pos.SetDispenserButton";

    setup() {
        this.pos = usePos();
        this.popup = useService("popup");
    }

    get currentOrder() {
        return this.pos.get_order();
    }

    get currentDispenserName() {
        return this.currentOrder.dispenser && this.currentOrder.dispenser.name || _t("Dispenser");
    }

    async selectDispenser(dispenserList) {
        if (!dispenserList.length) {
            return;
        }
        const { confirmed, payload: dispenser } = await this.popup.add(SelectionPopup, {
            title: _t("Change Dispenser"),
            list: dispenserList,
        });
        if (confirmed) {
            this.currentOrder.setDispenser(dispenser);
        }
    }

    async click() {
        var self = this;
        const selectionList = this.pos.pos_dispensers
            .filter((dispenser) => dispenser.available_in_pos)
            .map((dispenser) => {
                return {
                    id: dispenser.id,
                    item: dispenser,
                    label: dispenser.name,
                    isSelected: self.currentDispenserName == dispenser.name,
                };
            });

        this.selectDispenser(selectionList);
    }
}

ProductScreen.addControlButton({
    component: SetDispenserButton,
    condition: function () {
        const { config, pos_dispensers } = this.pos;
        return pos_dispensers.length > 0;
    },
    position: ["before", "OrderlineCustomerNoteButton"]
});
