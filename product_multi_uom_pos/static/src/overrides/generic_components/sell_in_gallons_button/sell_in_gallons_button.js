/** @odoo-module **/

import { _t } from "@web/core/l10n/translation";
import { OrderlineCustomerNoteButton } from "@point_of_sale/app/screens/product_screen/control_buttons/customer_note_button/customer_note_button";
import { patch } from "@web/core/utils/patch";

patch(OrderlineCustomerNoteButton.prototype, {
    onClickSellInGallons() {
        let selectedOrderline = this.pos.get_order().get_selected_orderline();
        if (!selectedOrderline) {
            return;
        }
        const uoms = selectedOrderline.getUom(selectedOrderline, this.pos);
        if (!uoms || uoms.length < 2) {
            return;
        }
        let nextUom = [uoms[1].price, uoms[1].uom_id[0], uoms[1].uom_id[1]];
        nextUom = nextUom.join(",");
        selectedOrderline = selectedOrderline.onSelectionChangedUom({
            "target": {
                "value": nextUom
            }
        });
        this.pos.get_order().exported_order_for_printing = selectedOrderline.order.export_for_printing();
    }
});
