/** @odoo-module */

import { Orderline } from "@point_of_sale/app/generic_components/orderline/orderline";
import { useRef } from "@odoo/owl";
import { patch } from "@web/core/utils/patch";

patch(Orderline.prototype, {
    setup(_defaultObj, options) {
        super.setup(...arguments);
        this.select_uom = useRef("uom_value");
        const currentOrder = this.env.services.pos.get_order();
        const selectedLine = currentOrder.get_last_orderline();
        const items = this.props.line.getUom(this);
        const defaultUom = items[0];
        if (selectedLine?.product_uom_id[0] === defaultUom?.uom_id[0]) {
            return;
        }
        try {
            if (items.length > 0) {
                const changeUom = (uomValueArray) => {
                    let price = uomValueArray[0];
                    var uomId = uomValueArray[1];
                    var uomName = uomValueArray[2];
                    if (uomName === "L" && !!currentOrder.partnerLitresPrice) {
                        price = currentOrder.partnerLitresPrice;
                    }
                    else if (uomName === "gal" && !!currentOrder.partnerGallonsPrice) {
                        price = currentOrder.partnerGallonsPrice;
                    }
                    selectedLine.product_uom_id = { 0: uomId, 1: uomName };
                    // Set the price_manually_set flag to indicate that the price was manually set
                    selectedLine.price_manually_set = true;
                    // Set the unit price of selected UoM on the order line
                    try {
                        selectedLine.set_unit_price(price);
                        currentOrder.removeOrderline(currentOrder.get_last_orderline());
                        currentOrder.add_orderline(selectedLine);
                        this.env.services.pos.set_order(currentOrder);
                    }
                    catch (error) {
                        console.dir(error);
                    }
                };
                changeUom([defaultUom.price, defaultUom.uom_id[0], defaultUom.uom_id[1]]);
            }
        }
        catch (err) {
            console.dir(err);
        }
    },
});

Orderline.props = {
    ...Orderline.props,
    line: {
        shape: {
            resetUom: { type: Function, optional: true },
            getUom: { type: Function, optional: true },
            onSelectionChangedUom: { type: Function, optional: true },
        },
    },
};