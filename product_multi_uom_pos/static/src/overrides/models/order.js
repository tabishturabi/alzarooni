/** @odoo-module **/

import { Order } from "@point_of_sale/app/store/models";
import { _t } from "@web/core/l10n/translation";
import { patch } from "@web/core/utils/patch";

patch(Order.prototype, {
    setup(_defaultObj, options) {
        super.setup(...arguments);
        this.exported_order_for_printing = null;
    },

    adjustUomAndPrice() {
        const selectedOrderline = this.get_selected_orderline();
        if (!selectedOrderline) {
            return;
        }
        const uoms = selectedOrderline.getUom(selectedOrderline, this.pos);
        if (!uoms || uoms.length < 2) {
            return;
        }

        let nextUom = uoms.find(uom => Number(uom.uom_id[0]) === Number(selectedOrderline.product_uom_id[0])) ?? uoms[0];
        nextUom = [nextUom.price, nextUom.uom_id[0], nextUom.uom_id[1]];
        nextUom = nextUom.join(",");
        selectedOrderline.onSelectionChangedUom({
            "target": {
                "value": nextUom
            }
        });
        if (selectedOrderline?.order?.export_for_printing) {
            this.exported_order_for_printing = selectedOrderline.order.export_for_printing();
        }
        return selectedOrderline;
    },

    updatePricelistAndFiscalPosition(newPartner) {
        let newPartnerPricelist, newPartnerFiscalPosition;
        const defaultFiscalPosition = this.pos.fiscal_positions.find(
            (position) => position.id === this.pos.config.default_fiscal_position_id[0]
        );
        if (newPartner) {
            newPartnerFiscalPosition = newPartner.property_account_position_id
                ? this.pos.fiscal_positions.find(
                    (position) => position.id === newPartner.property_account_position_id[0]
                )
                : defaultFiscalPosition;
            newPartnerPricelist =
                this.pos.pricelists.find(
                    (pricelist) => pricelist.id === newPartner.property_product_pricelist[0]
                ) || this.pos.default_pricelist;
        } else {
            newPartnerFiscalPosition = defaultFiscalPosition;
            newPartnerPricelist = this.pos.default_pricelist;
        }
        this.set_fiscal_position(newPartnerFiscalPosition);
        this.set_pricelist(newPartnerPricelist);
        this.adjustUomAndPrice();
    },

    setDispenser(dispenser) {
        this.dispenser = dispenser;
    }
});
