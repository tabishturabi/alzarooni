/** @odoo-module */

import { patch } from "@web/core/utils/patch";
import { Orderline } from "@point_of_sale/app/store/models";

patch(Orderline.prototype, {
    export_as_JSON() {
        var json = super.export_as_JSON.call(this);
        // Check if the product_uom_id is undefined.If yes, set product_uom_id to the default uom_id of the product
        if (this.product_uom_id == undefined) {
            this.product_uom_id = this.product.uom_id;
        }
        // Set the product_uom_id in the JSON object
        json.product_uom_id = this.product_uom_id[0];
        return json;
    },
    init_from_JSON(json) {
        super.init_from_JSON(...arguments);
        // Set the product_uom_id from the JSON data
        this.product_uom_id = {
            0: this.pos.units_by_id[json.product_uom_id].id,
            1: this.pos.units_by_id[json.product_uom_id].name,
        };
    },
    // Add a custom set_uom method
    set_uom(uom_id) {
        this.product_uom_id = uom_id;
    },
    // Override the get_unit method to get selected UoM from POS
    get_unit() {
        if (this.product_uom_id) {
            var unit_id = this.product_uom_id[0];
            if (!unit_id) {
                return undefined;
            }
            if (!this.pos) {
                return undefined;
            }
            return this.pos.units_by_id[unit_id];
        }
        return this.product.get_unit();
    },
    onSelectionChangedUom(ev) {
        var splitTargetValue = ev.target.value.split(',')
        const currentOrder = this.env.services.pos.get_order();
        let selectedLine = currentOrder.get_last_orderline().clone();
        selectedLine = Object.assign(selectedLine, currentOrder.get_last_orderline());
        selectedLine.order = currentOrder;
        currentOrder.removeOrderline(currentOrder.get_last_orderline());
        const changeUom = (uomValueArray) => {
            let price = uomValueArray[0];
            var uomId = uomValueArray[1];
            var uomName = uomValueArray[2];
            if (uomName === "L" && !!currentOrder.partnerLitresPrice) {
                price = currentOrder.partnerLitresPrice;
            }
            else if (uomName === "gal" && !!currentOrder.partnerGallonsPrice) {
                price = currentOrder.partnerGallonsPrice;
            }
            selectedLine.product_uom_id = { 0: uomId, 1: uomName };
            // Set the price_manually_set flag to indicate that the price was manually set
            selectedLine.price_manually_set = true;
            // Set the unit price of selected UoM on the order line
            try {
                // set_unit_price(price, selectedLine, this.env);
                selectedLine.set_unit_price(price);
                currentOrder.add_orderline(selectedLine);
                this.env.services.pos.set_order(currentOrder);
            }
            catch (error) {
                console.dir(error);
            }
        };
        changeUom(splitTargetValue);
        return selectedLine;
    },
    getUom(self, defaultPos = null) {
        const currentLine = (defaultPos ?? self.env.services.pos).get_order().get_selected_orderline();
        const uom = currentLine.product.pos_multi_uom_ids;
        const filteredData = (defaultPos ?? self.env.services.pos).pos_multi_uom.filter(obj => currentLine.product.pos_multi_uom_ids.includes(obj.id));
        return filteredData;
    },
    resetUom() {
        this.select_uom.el.value = 'change_uom';
        const currentOrder = this.env.services.pos.get_order();
        currentOrder.selected_orderline.set_uom({ 0: currentOrder.selected_orderline.product.uom_id[0], 1: currentOrder.selected_orderline.product.uom_id[1] })
        currentOrder.selected_orderline.set_unit_price(currentOrder.selected_orderline.product.lst_price);
    },
    getDisplayData() {
        return {
            ...super.getDisplayData(),
            getUom: this.getUom,
            resetUom: this.resetUom,
            onSelectionChangedUom: this.onSelectionChangedUom,
            vehicleNo: this.vehicleNo,
            driverNo: this.driverNo,
        };
    },
    set_quantity(quantity, keep_price) {
        var res = super.set_quantity(...arguments);
        const uom = this.product.pos_multi_uom_ids;
        if(uom.length > 0 && this.product_uom_id){
            const filteredUom = this.pos.pos_multi_uom.filter(obj => this.product.pos_multi_uom_ids.includes(obj.id));
            var product_uom = filteredUom.filter(obj => parseInt(this.product_uom_id[0]) === obj.uom_id[0])
            if(product_uom.length > 0){
                var uomValueArray = [product_uom[0].price, product_uom[0].uom_id[0], product_uom[0].uom_id[1]]
                this.set_uom_price(uomValueArray)
            }
        }
        return res
    },
    set_uom_price(uomValueArray){
        const currentOrder = this.order;
        let price = uomValueArray[0];
        var uomId = uomValueArray[1];
        var uomName = uomValueArray[2];
        if (uomName === "L" && !!currentOrder.partnerLitresPrice) {
            price = currentOrder.partnerLitresPrice;
        }
        else if (uomName === "gal" && !!currentOrder.partnerGallonsPrice) {
            price = currentOrder.partnerGallonsPrice;
        }
//        selectedLine.product_uom_id = { 0: uomId, 1: uomName };
        // Set the price_manually_set flag to indicate that the price was manually set
        this.price_manually_set = true;
        try {
            this.set_unit_price(price);
        }
        catch (error) {
            console.dir(error);
        }
    },
});
