/** @odoo-module */

import { ControlButtonsMixin } from "@point_of_sale/app/utils/control_buttons_mixin";
import { registry } from "@web/core/registry";
import { useService } from "@web/core/utils/hooks";
import { useBarcodeReader } from "@point_of_sale/app/barcode/barcode_reader_hook";
import { parseFloat } from "@web/views/fields/parsers";
import { _t } from "@web/core/l10n/translation";

import { NumberPopup } from "@point_of_sale/app/utils/input_popups/number_popup";
import { ErrorPopup } from "@point_of_sale/app/errors/popups/error_popup";
import { ControlButtonPopup } from "@point_of_sale/app/screens/product_screen/control_buttons/control_buttons_popup";
import { ConnectionLostError } from "@web/core/network/rpc_service";

import { usePos } from "@point_of_sale/app/store/pos_hook";
import { Component, onMounted, useExternalListener, useState } from "@odoo/owl";
import { ErrorBarcodePopup } from "@point_of_sale/app/barcode/error_popup/barcode_error_popup";

import { Numpad } from "@point_of_sale/app/generic_components/numpad/numpad";
import { ProductsWidget } from "@point_of_sale/app/screens/product_screen/product_list/product_list";
import { ActionpadWidget } from "@point_of_sale/app/screens/product_screen/action_pad/action_pad";
import { Orderline } from "@point_of_sale/app/generic_components/orderline/orderline";
import { OrderWidget } from "@point_of_sale/app/generic_components/order_widget/order_widget";

import { patch } from "@web/core/utils/patch";
import { ProductScreen } from "@point_of_sale/app/screens/product_screen/product_screen";

patch(ProductScreen.prototype, {
    adjustUomAndPrice() {
        const selectedOrderline = this.pos.get_order().get_selected_orderline();
        if (!selectedOrderline) {
            return;
        }
        const uoms = selectedOrderline.getUom(selectedOrderline, this.pos);
        if (!uoms || uoms.length < 2) {
            return;
        }

        let nextUom = uoms.find(uom => Number(uom.uom_id[0]) === Number(selectedOrderline.product_uom_id[0])) ?? uoms[0];
        nextUom = [nextUom.price, nextUom.uom_id[0], nextUom.uom_id[1]];
        nextUom = nextUom.join(",");
        if (selectedOrderline?.order?.export_for_printing) {
            this.pos.get_order().exported_order_for_printing = selectedOrderline.order.export_for_printing();
        }
        selectedOrderline.onSelectionChangedUom({
            "target": {
                "value": nextUom
            }
        });
    },

    onNumpadClick(buttonValue) {
        super.onNumpadClick(buttonValue);
        if (this.pos.get_order()?.export_for_printing) {
            const order = this.pos.get_order();
            setTimeout(() => {
                order.exported_order_for_printing = order.export_for_printing();
            }, 1000);
        }
    },

    _setValue(val) {
        const { numpadMode } = this.pos;
        const selectedLine = this.currentOrder.get_selected_orderline();
        if (selectedLine) {
            if (numpadMode === "quantity") {
                if (val === "remove") {
                    this.currentOrder.removeOrderline(selectedLine);
                } else {
                    const result = selectedLine.set_quantity(val);
                    if (!result) {
                        this.numberBuffer.reset();
                    }
                    this.adjustUomAndPrice();
                }
            } else if (numpadMode === "discount") {
                selectedLine.set_discount(val);
            } else if (numpadMode === "price") {
                selectedLine.price_type = "manual";
                selectedLine.set_unit_price(val);
            }
        }
    }
});
