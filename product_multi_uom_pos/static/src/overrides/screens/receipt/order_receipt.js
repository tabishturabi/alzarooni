/** @odoo-module */

import { OrderReceipt } from "@point_of_sale/app/screens/receipt_screen/receipt/order_receipt";
import { useRef } from "@odoo/owl";
import { patch } from "@web/core/utils/patch";

import { Component } from "@odoo/owl";
import { Orderline } from "@point_of_sale/app/generic_components/orderline/orderline";
import { OrderWidget } from "@point_of_sale/app/generic_components/order_widget/order_widget";
import { ReceiptHeader } from "@point_of_sale/app/screens/receipt_screen/receipt/receipt_header/receipt_header";
import { omit } from "@web/core/utils/objects";
import { usePos } from "@point_of_sale/app/store/pos_hook";


patch(OrderReceipt.prototype, {
    setup() {
        this.pos = usePos();
        if (this.pos.get_order().exported_order_for_printing) {
            this.props.data = this.pos.get_order().exported_order_for_printing;        }
    }
});
