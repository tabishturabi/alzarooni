/** @odoo-module */

import { PosStore } from "@point_of_sale/app/store/pos_store";
import { Order } from "@point_of_sale/app/store/models";
import { patch } from "@web/core/utils/patch";


patch(Order.prototype, {
    export_for_printing() {
        const result = super.export_for_printing(...arguments);
        console.log(this.partner)
        result.headerData.partnerData = this.partner;
        if (result.orderlines)
        {
        for (var key in result.orderlines){
        if (result.orderlines[key].driverNo)
        {
        result.headerData.driverNo = result.orderlines[key].driverNo
        }
        else
        {
        result.headerData.driverNo = ""

        }
                if (result.orderlines[key].vehicleNo)
        {
        result.headerData.vehicleNo = result.orderlines[key].vehicleNo
        }
        else
        {
        result.headerData.vehicleNo = ""

        }
        };


        }

        debugger;
        return result;
    },
});
